import React, { Component } from 'react'
import T from 'prop-types'
import { connect } from 'react-redux'
import moment from 'moment'

import {
  getMemberById,
  getMember
} from '../store/modules/members'
import Loading from '../components/misc/Loading'
import Avatar from '../components/misc/Avatar'

/**
 * Member Route
 */
class MemberRoute extends Component {
  static propTypes = {
    // Redux connected props
    getMember: T.func,
    member: T.object,
    isLoadingMember: T.bool,

    // React Router props
    match: T.object,
    history: T.object,
    location: T.object,
  }

  loadMember = () => {
    // Load the member if we haven't already
    if (this.props.member && !this.props.member.name) {
      this.props.getMember(this.props.member.login)
    }
  }

  componentDidMount() {
    this.loadMember()
  }

  componentDidUpdate() {
    this.loadMember()
  }

  render() {
    const { member } = this.props

    if (!member || this.props.isLoadingMember) {
      return <Loading />
    }

    return (
      <div>
        <h1>{member.name}</h1>
        <Avatar src={ member.avatar_url } alt={`${member.login}'s avatar`} />
        <div><strong>Location:</strong> {member.location}</div>
        <div><strong>Member Since:</strong> {moment(member.created_at).calendar()}</div>
        <div><strong>Email:</strong> {member.email}</div>
      </div>
    )
  }
}

const mapDispatchToProps = {
  getMember
}

const mapStateToProps = (state, ownProps) => ({
  member: getMemberById(state.members, ownProps.match.params.memberId),
  isLoadingMember: state.members.isLoadingIndividual
})

export default connect(mapStateToProps, mapDispatchToProps)(MemberRoute)
