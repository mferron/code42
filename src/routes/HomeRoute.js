import React, { Component } from 'react'

/**
 * Home Route
 */
export default class HomeRoute extends Component {
  render() {
    return (
      <div>
        <h1><span role="img" aria-label="Hello">👋</span></h1>
      </div>
    )
  }
}
