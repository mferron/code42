import BaseApi from './BaseApi'

const ORG_NAME = 'code42'

const ORG_MEMBERS = `/orgs/${ORG_NAME}/members`
const MEMBER_DETAILS = `/users/`

export class GithubApi extends BaseApi {
  getAuthToken = () => {
    return '5a762b1abd5007e966b7b07a081ea081ff49c339'
  }

  async getOrgMembers() {
    try {
      return await this.get(ORG_MEMBERS)
    } catch (error) {
      throw new Error('Unable to get members')
    }
  }

  async getMemberDetails(username) {
    try {
      return await this.get(MEMBER_DETAILS + username)
    } catch (error) {
      throw new Error('Unable to get member details')
    }
  }
}

export default new GithubApi({
  baseURL: 'https://api.github.com'
})
