import axios from 'axios'

const DEFAULT_CONFIG = {
  baseURL: ''
}

export default class BaseApi {
  constructor(config = DEFAULT_CONFIG) {
    this.config = config
    this.httpClient = axios.create({
      baseURL: this.config.baseURL
    })
  }

  _getHeaders() {
    const token = this.getAuthToken()
    if (token) {
      const Authorization = `token ${token}`
      return { Authorization, ...this.config.headers }
    } else {
      return this.config.headers
    }
  }

  _callWithHeaders(options) {
    return this.httpClient({
      ...options,
      headers: {
        ...this._getHeaders()
      },
    })
  }

  encodeQueryString(params) {
    return '?' + Object.keys(params).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
  }

  async get(url, data, requestConfig = this.config) {
    let urlWithParams = url
    if (data) {
      urlWithParams = url + this.encodeQueryString(data)
    }

    const response = await this._callWithHeaders({ method: 'get', url: urlWithParams, ...requestConfig })
    return response.data
  }

  async put(url, data) {
    const response = await this._callWithHeaders({ method: 'put', url, data })
    return response
  }

  async delete(url) {
    const response = await this._callWithHeaders({ method: 'delete', url })
    return response
  }

  async patch(url, data) {
    const response = await this._callWithHeaders({ method: 'patch', url, data })
    return response
  }

  async post(url, data) {
    const response = await this._callWithHeaders({ method: 'post', url, data })
    return response
  }
}
