import React, { Component } from 'react'
import { Switch } from 'react-router'
import { Provider } from 'react-redux'
import { BrowserRouter, Route } from 'react-router-dom'

import Layout from './components/Layout'
import HomeRoute from './routes/HomeRoute'
import './App.css'

import store from './store'

import MemberRoute from './routes/MemberRoute'

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <BrowserRouter>
          <Layout>
            <Switch>
              <Route exact path='/member/:memberId' component={ MemberRoute } />
              <Route exact path='/' component={ HomeRoute } />
            </Switch>
          </Layout>
        </BrowserRouter>
      </Provider>
    )
  }
}

export default App
