import { combineReducers } from 'redux'
import { normalize, schema } from 'normalizr'

import GithubApi from '../../utils/GithubApi'

const memberSchema = new schema.Entity('members')

//
// Actions
//
const SET_MEMBER = 'member/set'
const SET_MEMBERS = 'member/set-all'
const SET_IDS = 'member/ids'
const SET_MEMBER_ISLOADING = 'member/is-loading'
const SET_MEMBER_ISNOTLOADING = 'member/is-not-loading'
const SET_MEMBERS_ISLOADING = 'member/is-loading/all'
const SET_MEMBERS_ISNOTLOADING = 'member/is-not-loading/all'

//
// Reducers
//

/**
 * Reducer representing a list of member Ids stored in state
 *
 * @param {Object} state
 * @param {Object} action
 */
function ids(state = [], action = {}) {
  switch (action.type) {
  case SET_IDS:
    return action.payload
  default:
    return state
  }
}

/**
 * Reducer representing a hash of members stored by their id
 *
 * @param {Object} state
 * @param {Object} action
 */
function byId(state = {}, action = {}) {
  switch (action.type) {
  case SET_MEMBERS: {
    return action.payload
  }
  case SET_MEMBER:
    return {
      ...state,
      [action.payload.id]: action.payload
    }
  default:
    return state
  }
}

/**
 * Reducer representing the loading state of member(s)
 *
 * @param {Boolean} state
 * @param {Object} action
 */
function isLoadingReducer(state = false, action = {}) {
  switch (action.type) {
  case SET_MEMBERS_ISLOADING:
    return true
  case SET_MEMBERS_ISNOTLOADING:
    return false
  default:
    return state
  }
}

/**
 * Reducer representing the loading state of an individual member
 *
 * @param {Boolean} state
 * @param {Object} action
 */
function isLoadingIndividualReducer(state = false, action = {}) {
  switch (action.type) {
  case SET_MEMBER_ISLOADING:
    return true
  case SET_MEMBER_ISNOTLOADING:
    return false
  default:
    return state
  }
}

export default combineReducers({
  ids,
  byId,
  isLoading: isLoadingReducer,
  isLoadingIndividual: isLoadingIndividualReducer
})

//
// Selectors
//

/**
 * Get an member from state by id
 *
 * @param {Object} state
 * @param {String} memberId
 * @returns {Object}
 */
export const getMemberById = (state, memberId) => state.byId[memberId]

/**
 * Get all member in state
 *
 * @param {Object} state
 * @returns {Array<Object>}
 */
export const getAllMembers = (state) => state.ids.map((memberId) => getMemberById(state, memberId))

//
// Action creators
//

function setMember(member) {
  return {
    type: SET_MEMBER,
    payload: member
  }
}

function setMembers(members) {
  return {
    type: SET_MEMBERS,
    payload: members
  }
}

function setIds(ids) {
  return {
    type: SET_IDS,
    payload: ids
  }
}

function setLoadingList(value) {
  return {
    type: value ? SET_MEMBERS_ISLOADING : SET_MEMBERS_ISNOTLOADING
  }
}

function setLoadingIndividual(value) {
  return {
    type: value ? SET_MEMBER_ISLOADING : SET_MEMBER_ISNOTLOADING
  }
}

//
// Thunks
//

export function getMember(username) {
  return async (dispatch) => {
    try {
      dispatch(setLoadingIndividual(true))
      const result = await GithubApi.getMemberDetails(username)
      const normalizedResult = normalize(result, memberSchema)

      // assumes a member list has been retrieved already

      dispatch(setMember(normalizedResult.entities.members[normalizedResult.result]))
      dispatch(setLoadingIndividual(false))
    } catch (err) {
      dispatch(setLoadingIndividual(false))
    }
  }
}

export function getOrgMembers() {
  return async (dispatch) => {
    try {
      dispatch(setLoadingList(true))
      const result = await GithubApi.getOrgMembers()
      const normalizedResult = normalize(result, [memberSchema])

      dispatch(setMembers(normalizedResult.entities.members))
      dispatch(setIds(normalizedResult.result))
      dispatch(setLoadingList(false))
    } catch (err) {
      dispatch(setLoadingList(false))
    }
  }
}
