import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'

import membersReducer from './modules/members'

const rootReducer = combineReducers({
  members: membersReducer
})

let store = createStore(
  rootReducer,
  applyMiddleware(thunk)
)

export default store
