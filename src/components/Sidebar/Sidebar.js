import React, { Component } from 'react'
import T from 'prop-types'
import { NavLink } from 'react-router-dom'

import Loading from '../misc/Loading'

/**
 * Sidebar component
 */
class Sidebar extends Component {
  static propTypes = {
    members: T.array,
    isLoading: T.bool
  }

  render() {
    return (
      <nav className="app-nav">
        <h2>Public Members</h2>
        { this.props.isLoading ? <Loading /> :
          <div className="list-group">
            { this.props.members.map((members, i) => {
              return (
                <NavLink key={`members-${i}`}
                  to={`/member/${members.id}`}
                  className="list-group-item list-group-item-action">
                  {members.login}
                </NavLink>
              )
            })}
          </div>
        }
      </nav>
    )
  }
}

export default Sidebar
