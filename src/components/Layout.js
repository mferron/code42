import React, { Component } from 'react'
import { withRouter } from 'react-router'
import T from 'prop-types'
import { connect } from 'react-redux'

import Header from './Header/Header'

import {
  getAllMembers,
  getOrgMembers
} from '../store/modules/members'

import Sidebar from './Sidebar/Sidebar'

/**
 * Layout component - wraps the entire application
 */
class Layout extends Component {
  static propTypes = {
    children: T.element.isRequired,

    getOrgMembers: T.func,
    members: T.array,
    isLoadingMembers: T.bool
  }

  componentDidMount() {
    this.props.getOrgMembers()
  }

  render() {
    return (
      <div className="app">
        <header className="app-header">
          <Header />
        </header>
        <div className="app-body">
          <main className="app-content">
            { this.props.children }
          </main>
          <Sidebar isLoading={this.props.isLoadingMembers} members={this.props.members} />
        </div>
        <footer className="app-footer"></footer>
      </div>
    )
  }
}

const mapDispatchToProps = {
  getOrgMembers
}

const mapStateToProps = (state) => ({
  members: getAllMembers(state.members),
  isLoadingMembers: state.members.isLoading
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout))
