import styled from 'styled-components'

export default styled.img`
  max-width: 200px;
`
