import styled, { keyframes } from 'styled-components'

const rotate = keyframes`
  100% {
    transform: rotate(360deg);
  }
`

export default styled.div`
  height: 0;
  width: 0;
  padding: 15px;
  border: 6px solid #cccccc;
  border-right-color: #888888;
  border-radius: 22px;
  animation: ${rotate} 1s infinite linear;

  margin: 0 auto;
`
