import React from 'react'

import styles from './Header.module.css'

/**
 * Header
 */
const Header = () => (
  <div className={styles.header}>
    <div>Code 42 Exercise</div>
  </div>
)

export default Header
